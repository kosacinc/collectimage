package kr.smartauth.collectImage.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kr.smartauth.collectImage.entity.Agree;

public interface AgreeRepository extends JpaRepository<Agree, Long> {

}
