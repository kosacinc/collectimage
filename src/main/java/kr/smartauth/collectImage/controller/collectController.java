package kr.smartauth.collectImage.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import kr.smartauth.collectImage.entity.Agree;
import kr.smartauth.collectImage.repository.AgreeRepository;

@RestController
@RequestMapping("/collect/v1")
public class collectController {
	
	@Autowired
	private AgreeRepository agreeRepository;
	
	@Value("${app.outputDir}")
	private String outputDir;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping
	public ModelAndView index() {
		return new ModelAndView("/consent");
	}
	
	@RequestMapping(value = "/image", method = RequestMethod.POST)
	public ModelAndView collect(@RequestParam("radio-yes") String answer, @RequestParam(name = "fileToUpload", required = true) MultipartFile file) throws IOException {
		
		Agree agree = new Agree();
		agree.setAgree(answer);
		agree.setFilename(file.getOriginalFilename());
		
		agree = agreeRepository.save(agree);
		
		if (!file.isEmpty() && "yes".equals(answer)) {
			
			String name = String.format("%08d.", agree.getId()) + getExtension(file.getOriginalFilename());
			logger.info("*** collect/image save id={} name={}", agree.getId(), name);
			
			OutputStream out = null;
			try {
				out = new FileOutputStream(outputDir + "/" + name);
				out.write(file.getBytes());
			} finally {
				if (out != null) {
					try {
						out.close();
					} catch (IOException e) {
						logger.error("Exception while close " + name, e);
					}
				}
			}
		} else if (file.isEmpty()) {
			logger.info("*** collect/image save id={} answer={} empty_image", agree.getId(), answer);
		} else {
			logger.info("*** collect/image save id={} answer={} not_save_image", agree.getId(), answer);
		}
		
		return new ModelAndView("/finish");
	}
	
	private String getExtension(String name) {
		int pos = name.lastIndexOf('.');
		if (pos < 0) {
			return "";
		} else {
			return name.substring(pos + 1);
		}
	}
}
