package kr.smartauth.collectImage.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
public class Agree implements Serializable {

	private static final long serialVersionUID = -8796286095200781345L;

	@Id
	@GeneratedValue(generator = "AgreeSequenceGenerator")
	@GenericGenerator(
			name = "AgreeSequenceGenerator",
			strategy = "native",
			parameters = {
				@Parameter(name = "prefer_sequence_per_entity", value = "true")
			})
	private Long id;
	
	private String agree;
	private String filename;
	private Timestamp createdDTM;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgree() {
		return agree;
	}

	public void setAgree(String agree) {
		this.agree = agree;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Timestamp getCreatedDTM() {
		return createdDTM;
	}

	public void setCreatedDTM(Timestamp createdDTM) {
		this.createdDTM = createdDTM;
	}

	@PrePersist
	public void prePersist() {
		this.createdDTM = new Timestamp(System.currentTimeMillis());
	}
}
